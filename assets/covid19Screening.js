var defaultOrder = {
	"covid-19": 1,
	"flu": 2,
	"common cold": 3,
	"allergies": 4
}

var freqProb = {
	"common": 0.8,
	"sometimes": 0.4,
	"rare": 0.1,
	"no": 0.05,
	"mild": 0.2
}

var Pd = {
	"covid-19": 0.25,
	"common cold": 0.25,
	"flu": 0.25,
	"allergies": 0.25
}

var modifiers = ['worsening symptoms', 'exposure to known covid-19 patient', 'history of travel'];

var scoreData = {
	"covid-19": {
		"fever": "common",
		"shortness of breath": "common",
		"itchy or watery eyes": "no",
		"dry cough": "common",
		"headaches": "sometimes",
		"aches and pains": "sometimes",
		"sore throat": "sometimes",
		"fatigue": "sometimes",
		"diarrhea": "rare",
		"runny or stuffy nose": "rare",
		"sneezing": "no",
		"vomiting": "rare",
		"history of travel": "common",
		"exposure to known covid-19 patient": "common",
		"worsening symptoms": "common"
	},
	"common cold": {
		"fever": "rare",
		"shortness of breath": "no",
		"itchy or watery eyes": "rare",
		"dry cough": "mild",
		"headaches": "rare",
		"aches and pains": "common",
		"sore throat": "common",
		"fatigue": "sometimes",
		"diarrhea": "no",
		"runny or stuffy nose": "common",
		"sneezing": "common",
		"vomiting": "no",
		"history of travel": "sometimes",
		"exposure to known covid-19 patient": "sometimes",
		"worsening symptoms": "rare"
	},
	"flu": {
		"fever": "common",
		"shortness of breath": "no",
		"itchy or watery eyes": "no",
		"dry cough": "common",
		"headaches": "common",
		"aches and pains": "common",
		"sore throat": "common",
		"fatigue": "common",
		"diarrhea": "sometimes",
		"runny or stuffy nose": "sometimes",
		"sneezing": "no",
		"vomiting": "sometimes",
		"history of travel": "sometimes",
		"exposure to known covid-19 patient": "sometimes",
		"worsening symptoms": "common"
	},
	"allergies": {
		"fever": "sometimes",
		"shortness of breath": "common",
		"itchy or watery eyes": "common",
		"dry cough": "sometimes",
		"headaches": "sometimes",
		"aches and pains": "no",
		"sore throat": "no",
		"fatigue": "sometimes",
		"diarrhea": "no",
		"runny or stuffy nose": "common",
		"sneezing": "common",
		"vomiting": "no",
		"history of travel": "sometimes",
		"exposure to known covid-19 patient": "sometimes",
		"worsening symptoms": "rare"
	}
}

function updateUrlHash() {
    var hashParamsArr = [];

    $.each($("#main-table input[type='checkbox']:checked"), function() {
    	hashParamsArr.push($(this).prop('name'));
	});
	hashParamsArr.push('_');

    window.location.replace(('' + window.location).split('#')[0] + '#' + hashParamsArr.join('&'));
};

function updateFromHashParams() {
    var hashParamsArray = window.location.hash.substring(1).split('&');
   
    for (var i=0; i<hashParamsArray.length; i++) {
    	var param = decodeURIComponent(hashParamsArray[i]);
    	$('input[name="' + param + '"]') && $('input[name="' + param + '"]').parent().checkbox('check');
     }

     if ((hashParamsArray.length > 0) && (hashParamsArray[0].length > 0)) {
     	onCheck('hash');
     }
};


function populateStrings() {
	var str = locales[lang];
	if (lang === 'he') {
		$('body').addClass('rtl');
	}
	$('#header h1').text(str['page-title']);
	$('#header h4:eq(0)').text(str['page-subtitle1']);
	$('#disclaimer p').html(str['disclaimer']);
	$('#symptom-th').text(str['symptom']);
	$('#covid19-th').text(str.diseases['covid-19']);
	$('#common-cold-th').text(str.diseases['common cold']);
	$('#flu-th').text(str.diseases['flu']);
	$('#allergies-th').text(str.diseases['allergies']);
	$('#cta').html(str['cta']);
	$('#tech-limits').html(str['tech-limits']);
	$('#about').text(str['about-us']);
	$('#terms').text(str['terms-of-use']);
	$('#privacy').text(str['privacy-policy']);
	$('#copyrights').text(str['copyrights']);
	$('#contact').text(str['contact-us']);
}

function buildMainTable() {
	var str = locales[lang];
	var symptoms = '';
	var row = 0;
	for (var symptom in str['symptoms']) {
		symptomStr = locales[lang].symptoms[symptom];
		symptoms += '<tr>';
		symptoms += '<td class="collapsing">';
		symptoms += '<div class="ui checkbox">';
        symptoms += '<input type="checkbox" name="' + symptom + '"><label>' + symptomStr + '</lablel>';
        symptoms += '</div>';
		symptoms += '</td>';
      	for (var disease in defaultOrder) {
      		symptoms += '<td class=' + disease.replace(/ /g, '-') + '-td>' + str['frequencies'][scoreData[disease][symptom]] + '</td>';
      	}
      	symptoms += '</tr>';
	}
	$('#main-table tbody').append(symptoms);
}

function clearResults() {
//	mainTable.colReorder.order([0, 1, 2, 3, 4], true);
	for (disease in defaultOrder) {
		$('#' + disease.replace(/ /g, '-') + '-th').css('background-color', '');
		$('.' + disease.replace(/ /g, '-') + '-td').css('background-color', '');
	}
}

function getDiseasesProb() {
	var prob = {};
	var totalProb = 0;
	$.each($("#main-table input[type='checkbox']"), function() {
		var symptom = $(this).prop('name');
		for (var disease in defaultOrder) {
			if (typeof prob[disease] === 'undefined') prob[disease] = 1;
			if ($(this).is(":checked")) {
				prob[disease] *= freqProb[scoreData[disease][symptom]];
			} else {
				if ((symptom === 'fever') || (symptom === 'shortness of breath') || (symptom === 'itchy or watery eyes')) {
					prob[disease] *= (1-freqProb[scoreData[disease][symptom]]);
				}
			}
		}
	});

	console.log(prob);

	for (var disease in scoreData) {
		totalProb += prob[disease] * Pd[disease];
	}
	console.log(totalProb);

	for (var disease in scoreData) {
		prob[disease] = (prob[disease]*Pd[disease])/totalProb;
	}
	return prob;
}

function justModifiersAreChecked() {
	var totalSymptoms = 0;
	$.each($("#main-table input[type='checkbox']:checked"), function() {
		var symptom = $(this).prop('name');
		if (modifiers.indexOf(symptom) < 0) totalSymptoms++;
	});
	return totalSymptoms;
}

function onCheck(source) {
	updateUrlHash();

	if (($("#main-table input[type='checkbox']:checked").length === 0) ||
		(justModifiersAreChecked() === 0)) {
		clearResults();
		return;
	}

	if (justModifiersAreChecked() === 0) {
		return;
	}

	var prob = getDiseasesProb();

	console.log(prob);

	// Sort columns
	probSorted = Object.keys(prob).sort(function(a,b){return prob[b]-prob[a]});

	var newOrder = [0];
	for (var i=0; i<probSorted.length; i++) {
		newOrder.push(defaultOrder[probSorted[i]]);
	}

	// mainTable.colReorder.order(newOrder, true);

	// Set bg colors
	var baseProb = prob[probSorted[0]];
	for (disease in defaultOrder) {
		var currProb = prob[disease];
		var color = Math.round(255 * (1 - (currProb/baseProb)));
		$('#' + disease.replace(/ /g, '-') + '-th').css('background-color', 'rgba(255,'+ color + ',' + color + ', 0.8)');
		$('.' + disease.replace(/ /g, '-') + '-td').css('background-color', 'rgba(255,'+ color + ',' + color + ', 0.8)');
	}
}

function scrollToBottom() {
	var page = $("html, body");

	page.on("scroll mousedown wheel DOMMouseScroll mousewheel keyup touchmove", function(){
		page.stop();
	});

	page.animate({ scrollTop: $(document).height() }, 1000, function(){
		page.off("scroll mousedown wheel DOMMouseScroll mousewheel keyup touchmove");
	});
}

function adjustColumns() {
	$($.fn.dataTable.tables( true ) ).css('width', '100%');
	$($.fn.dataTable.tables( true ) ).DataTable().columns.adjust().draw();
}

$(document).ready(function () {
	populateStrings();

	$('#disclaimer-agree').on('click', function () {
		buildMainTable();

		$('#main-table .checkbox').checkbox({
			onChecked: onCheck,
			onUnchecked: onCheck
		});
		var fixedHeader = !(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent));

		mainTable = $('#main-table').DataTable({
			"bFilter": false,
			"bInfo": false,
			"bPaginate": false,
			"bLengthChange": false,
			"bSort": false,
			"colReorder": false,
			"fixedHeader": fixedHeader,
			"scrollX": false,
			'autoWidth': true
		});
	
		$( window ).resize(adjustColumns);
	
		setTimeout(function () {
			$('#disclaimer').hide();
			$('#main-table').show();
			$('#cta').show();
			$('#tech-limits').show();
			adjustColumns();
		}, 150);
	
		updateFromHashParams();	
	})
})
